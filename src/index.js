import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/app/App';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import {urlsPrivate} from "./utils/urlUtils";
import {Provider} from 'react-redux';
import * as reducers from '../src/store/reducer';
import watchSaveUser from '../src/store/user/sagas';
import {applyMiddleware, combineReducers, createStore} from "redux";
import createSagaMiddleware from 'redux-saga';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    combineReducers(reducers),
    applyMiddleware(sagaMiddleware)
);
sagaMiddleware.run(watchSaveUser);

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <Route path={urlsPrivate.home.path} component={App}/>
        </Router>
    </Provider>,
    document.getElementById('root')
);

