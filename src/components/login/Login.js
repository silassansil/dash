import React, {Component} from 'react';
import {withRouter} from "react-router-dom";
import {Button, TextField, Typography} from "material-ui";
import FirebaseService from "../../services/FirebaseService";
import {urlsPrivate} from "../../utils/urlUtils";

class Login extends Component {

    state = {
        email: '',
        senha: ''
    };

    login = (event) => {
        event.preventDefault();
        const {email, senha} = this.state;

        FirebaseService.login(email, senha)
            .then(() => {
                this.props.history.push(urlsPrivate.home.path);
            })
            .catch(error => {
                alert(error.message);
            });
    };

    createUser = async () => {
        this.props.history.push(urlsPrivate.novo.path)
    };

    alterarValor = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    render() {
        return (<React.Fragment>

            <div style={{marginLeft: '25%', marginRight: '25%',}}>
                <Typography variant="headline" component="h2">Login</Typography>
                <form onSubmit={this.login}>
                    <TextField className="input-field"
                               type="email"
                               value={this.state.email}
                               label="Email"
                               required
                               onChange={this.alterarValor('email')}/>
                    <TextField className="input-field"
                               type="password"
                               value={this.state.senha}
                               label="Senha"
                               required
                               onChange={this.alterarValor('senha')}/>

                    <Button type="submit"
                            variant="raised" color="primary"
                            style={{marginTop: '20px', display: 'inline-block'}}>
                        Login
                    </Button>

                    <Button variant="raised" color="primary"
                            style={{marginTop: '20px', display: 'inline-block'}}
                            onClick={() => this.createUser()}>
                        Registrar-se
                    </Button>

                </form>
            </div>
        </React.Fragment>)
    }
}

export default withRouter(Login);