import React, {Component} from 'react';
import './App.css';

import {MuiThemeProvider} from "material-ui/styles/index";
import {createMuiTheme} from 'material-ui/styles';
import Card from "material-ui/es/Card/Card";
import CardContent from "material-ui/es/Card/CardContent";
import {Route, withRouter} from 'react-router-dom';
import {urls, urlsPrivate} from "../../utils/urlUtils";
import {Welcome} from "../home/Home";
import TopBar from "../topBar/TopBar";
import ListagemUsuario from "../usuario/ListagemUsuario";
import Novo from "../usuario/Novo";
import blue from "material-ui/es/colors/blue";
import FirebaseService from "../../services/FirebaseService";
import {login, logout} from "../../action/actionCreator";
import {connect} from "react-redux";
import {compose} from "recompose";
import LoginFilter from "../../loginFilter/LoginFilter";
import Login from "../login/Login";

const theme = createMuiTheme({
    palette: {
        primary: blue,
    },
});

class App extends Component {

    componentDidMount() {
        FirebaseService.onAuthChange(
            (usuarioLogado) => this.props.login(usuarioLogado),
            () => this.props.logout()
        );
    }

    render() {
        return (
            <MuiThemeProvider theme={theme}>
                <React.Fragment>
                    <TopBar data={this.state}/>

                    <Card style={{margin: '50px'}}>
                        <CardContent>
                            <Route exact
                                   path={urlsPrivate.home.path}
                                   render={(props) =>
                                       <LoginFilter component={Welcome} {...props}/>}
                            />

                            <Route exact
                                   path={urls.usuario.path}
                                   render={(props) =>
                                       <LoginFilter component={ListagemUsuario} {...props}/>}
                            />

                            <Route exact
                                   path={urlsPrivate.novo.path}
                                   render={(props) =>
                                       <Novo {...props}/>}
                            />

                            <Route exact
                                   path={urlsPrivate.login.path}
                                   render={(props) =>
                                       <Login {...props}/>}
                            />
                        </CardContent>
                    </Card>
                </React.Fragment>
            </MuiThemeProvider>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        login: authUser => dispatch(login(authUser)),
        logout: () => dispatch(logout()),
    }
};

export default compose(
    withRouter,
    connect(null, mapDispatchToProps)
)(App);
