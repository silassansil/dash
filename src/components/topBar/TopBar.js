import React, {Component} from 'react';
import {urls, urlsPrivate} from "../../utils/urlUtils";
import {AppBar, Button, IconButton, Toolbar, Typography} from "material-ui";
import MenuIcon from "material-ui-icons/Menu";
import {Link, withRouter} from 'react-router-dom';
import {ArrowForward} from "material-ui-icons";
import {connect} from "react-redux";
import {compose} from "recompose";
import {logout} from "../../action/actionCreator";


class TopBar extends Component {

    botaoHome() {
        return(
            <IconButton color="inherit" aria-label="Menu"
                        component={props => <Link to={urlsPrivate.home.path} {...props}/>}
            >
                <MenuIcon/>
            </IconButton>
        );
    }

    render() {
        return(
            <AppBar position="static">
                <Toolbar>
                    { this.botaoHome() }
                    {/*{TODO Esconder menu sem auth}*/}
                    {
                        Object.values(urls).map((url, index) => {
                            return <Button key={index}
                                           color="inherit"
                                           component={props =>
                                               <Link to={url.path} {...props}/>
                                           }
                            >
                                {url.name}
                            </Button>
                        })
                    }

                    {
                        this.userAuth &&
                        <React.Fragment>
                            <Typography type="title" color="inherit" style={{marginLeft: '20px'}}>
                                {this.userAuth.email}
                            </Typography>

                            <IconButton color="inherit" aria-label="Menu" onClick={() => logout()}>
                                <ArrowForward/>
                            </IconButton>
                        </React.Fragment>
                    }

                </Toolbar>
            </AppBar>
        );
    }
}
const mapStateToProps = state => {
    return {userAuth: state.userAuth}
};

const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(logout()),
    }
};

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(TopBar);

