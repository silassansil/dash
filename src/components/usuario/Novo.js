import React, {Component} from 'react';

import {Button, TextField, Typography} from "material-ui";
import {withRouter} from "react-router-dom";
import FirebaseService from "../../services/FirebaseService";
import Checkbox from "material-ui/es/Checkbox/Checkbox";
import {connect} from "react-redux";

import {saveUserRequest} from '../../store/user/action';
import {compose} from "recompose";
import {urls} from "../../utils/urlUtils";

class Novo extends Component {

    state = {
        id: null,
        nome: '',
        email: '',
        ativo: true,
        senha: '',
    };

    alterarValor = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    salvarUsuario = async () => {

        // await FirebaseService.pushData('usuarios', this.state);
        this.props.history.push(urls.usuario.path);
    };
    submeterFormulario = (event) => {
        event.preventDefault();
        console.log("OK SAVE AUTH");
        this.props.saveUserRequest(this.state);
    };

    renderLoading = () => {
        return <p>Loading...</p>
    };

    render() {
        return (<React.Fragment>

            {this.props.loading && this.renderLoading()}
            <Typography variant="headline" component="h2">Novo usuário</Typography>
            <form onSubmit={this.submeterFormulario}>
                <TextField className="input-field"
                           type="text"
                           value={this.state.nome}
                           label="Nome:"
                           required
                           onChange={this.alterarValor('nome')}/>

                <TextField className="input-field"
                           type="email"
                           label="Email"
                           value={this.state.email}
                           required
                           onChange={this.alterarValor('email')}/>

                <TextField className="input-field"
                           type="password"
                           label="Senha"
                           value={this.state.senha}
                           required
                           onChange={this.alterarValor('senha')}/>

                <Checkbox className="input-field"
                          label="Ativo?"
                          value={this.state.ativo}
                          required
                          onChange={this.alterarValor('ativo')}/>

                <Button
                    type="submit"
                    variant="raised" color="primary"
                    style={{marginTop: '20px', display: 'inline-block'}}>
                    Salvar
                </Button>
            </form>
        </React.Fragment>)
    }
}

const mapDispatchToProps = dispatch => {
    return {
        saveUserRequest: user => dispatch(saveUserRequest(user))
    }
};

export default compose(
    withRouter,
    connect(null, mapDispatchToProps)
)(Novo);