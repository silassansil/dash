import React, {Component} from 'react';
import {Table, TableBody, TableCell, TableHead, TableRow, Typography} from "material-ui";
import Button from "material-ui/es/Button/Button";
import FirebaseService from "../../services/FirebaseService";
import {Link, withRouter} from "react-router-dom";
import {urlsPrivate} from "../../utils/urlUtils";


class ListagemUsuario extends Component {

    state = {
        data: []
    };

    componentWillMount() {
        FirebaseService.getList('usuarios', (dataReceived) => this.setState({data: dataReceived}))
    }

    removePorKey = (key) => {
        console.log("eNTROU");
        FirebaseService.removeByKey(key, 'usuarios');
    };

    render() {
        return <React.Fragment>
            <Typography variant="headline" component="h2">Usuário</Typography>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Nome</TableCell>
                        <TableCell>Email</TableCell>
                        <TableCell>Ativo?</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        this.state.data.map((item) =>
                            <TableRow key={item.key}>
                                <TableCell>{item.nome}</TableCell>
                                <TableCell>{item.email}</TableCell>
                                <TableCell>{item.ativo ? 'Sim' : 'Não'}</TableCell>
                                <TableCell>
                                    <Button onClick={() => this.removePorKey(item.key)}>
                                        Excluir
                                    </Button>
                                </TableCell>
                            </TableRow>
                        )
                    }
                </TableBody>
            </Table>

            <Button
                style={{marginTop: 20}}
                variant="raised" color="primary"
                component={props =>
                    <Link to={urlsPrivate.novo.path} {...props}/>
                }>
                {urlsPrivate.novo.name}
            </Button>
        </React.Fragment>
    }
}

export default withRouter(ListagemUsuario);