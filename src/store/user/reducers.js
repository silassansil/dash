import * as Types from "./actions-type";

const initialState = {
    data: {},
    loading: false,
    error: false,
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case Types.SAVE_USER_REQUEST:
            return {
                data: null,
                loading: true,
                error: false,
            };

        case Types.SAVE_USER_SUCCESS:
            console.log(JSON.stringify(action.data));
            return {
                data: action.data,
                loading: false,
                error: false,
            };
        default:
            return state
    }
};