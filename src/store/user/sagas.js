import {takeLatest, call} from "redux-saga/effects";
import * as Types from "./actions-type";
import FirebaseService from "../../services/FirebaseService";

function* watchSaveUser() {
    yield takeLatest(Types.SAVE_USER_REQUEST, saveUserAuth);
}

function* saveUserAuth({user}) {
    const data = yield call(FirebaseService.createUser(user.email, user.senha));
    return {
        data,
        type: Types.SAVE_USER_SUCCESS
    }
}

export default watchSaveUser;