import * as Types from './actions-type';

const saveUserRequest = (user) => {
    return {user, type: Types.SAVE_USER_REQUEST}
};

const saveUserSuccess = (data) => {
    return {data, type: Types.SAVE_USER_SUCCESS}
};

const saveUserFailed = (data) => {
    return {error: data.error, type: Types.SAVE_USER_FAILED}
};

export {
    saveUserRequest,
    saveUserSuccess,
    saveUserFailed
}

