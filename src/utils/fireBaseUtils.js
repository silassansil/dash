import * as firebase from 'firebase';


const config = {
    apiKey: "AIzaSyDW8B0NM9DW1jOYgN0biNbPH2XefM9vZ3s",
    authDomain: "dash-6f18a.firebaseapp.com",
    databaseURL: "https://dash-6f18a.firebaseio.com",
    projectId: "dash-6f18a",
    storageBucket: "dash-6f18a.appspot.com",
    messagingSenderId: "459665023965"
};

firebase.initializeApp(config);
const firebaseDatabase = firebase.database();
const firebaseAuth = firebase.auth();

export {
    firebaseDatabase,
    firebaseAuth
}