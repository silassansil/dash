export const urls = {
    usuario: {name: 'Usuario', path: '/usuario'},
};

export const urlsPrivate = {
    home: {name: 'Home', path: '/'},
    novo: {name: 'Novo Usuário', path: '/usuario/novo'},
    login: {name: 'Login', path: '/login'},
};
