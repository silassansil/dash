import {firebaseDatabase, firebaseAuth} from '../utils/fireBaseUtils';

export default class FirebaseService {
    static getList = (nodePath, callback, size = 10) => {
        const query = firebaseDatabase.ref(nodePath).limitToLast(size);
        query.on('value', dataSnapshot => {
            let items = [];
            dataSnapshot.forEach(childSnapshot => {
                let item = childSnapshot.val();
                item['key'] = childSnapshot.key;
                items.push(item);
            });
            callback(items);
        });

        return query;
    };


    static pushData = (node, objeto) => {
        const ref = firebaseDatabase.ref(node).push();
        const id = firebaseDatabase.ref(node).push().key;
        ref.set(objeto);
        return id;
    };

    static removeByKey = (key, node) => {
        return firebaseDatabase.ref(node + '/' + key).remove();
    };

    static createUser = (email, password) => {
        return firebaseAuth.createUserWithEmailAndPassword(email, password);
    };

    static login = (email, password) => {
        return firebaseAuth.signInWithEmailAndPassword(email, password);
    };

    static logout = () => {
        return firebaseAuth.signOut();
    };

    static onAuthChange = (callbackLogin, callbackLogout) => {
        firebaseAuth.onAuthStateChanged(usuarioLogado => {
            if (!usuarioLogado) {
                callbackLogout(usuarioLogado);
            } else {
                callbackLogin(usuarioLogado);
            }
        });
    };

}