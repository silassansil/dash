import {connect} from "react-redux";
import {Redirect, withRouter} from "react-router-dom";
import {compose} from "recompose";
import React from "react";
import {urlsPrivate} from "../utils/urlUtils";

const validarUsuarioLogado = (usuarioLogado, Component, props) => {
    return usuarioLogado
        ? <Component {...props}/>
        : <Redirect to={urlsPrivate.login.path}/>
};

const LoginFilter = ({userAuth, component, ...otherProps}) => {
    return validarUsuarioLogado(userAuth, component, otherProps);
};

const mapStateToProps = state => {
    return {userAuth: state.userAuth}
};

export default compose(withRouter, connect(mapStateToProps))(LoginFilter);

